pygame==1.9.4
pynput==1.4.2
python-xlib==0.25
rpi-ws281x==4.1.0
RPi.GPIO==0.6.5
six==1.12.0
smbus==1.1.post2
