import pygame
import game_colors

#consts
xsize = 1000
ysize = 1000

step_size = 5


init_radius = 100


def place_image(gameDisplay,img,x,y):
    gameDisplay.blit(img,(x,y))

#disc generator
def things(gameDisplay,xpos, ypos, Radius, color):
   pygame.draw.circle(gameDisplay,color,(xpos,ypos),int(Radius),1)

#game score counter

def score_counter(gameDisplay,count):
    font = pygame.font.SysFont(None, 25)
    text = font.render("Score: " + str(count), True, game_colors.black)
    gameDisplay.blit(text,(0,0))


def time_counter(gameDisplay,count):
    font = pygame.font.SysFont(None, 25)
    text = font.render("Time left: " + str(count), True, game_colors.black)
    gameDisplay.blit(text, (0, 30))


#generate image objects from requested font and text
def text_objects(gameDisplay,text, font):
    textSurface = font.render(text, True, game_colors.black)
    return textSurface, textSurface.get_rect()

def message_display(gameDisplay,text):
    largeText = pygame.font.Font('freesansbold.ttf',25) #import font style.
    TextSurf, TextRect = text_objects(gameDisplay,text, largeText) #generate text object
    TextRect.center = ((xsize/2),(ysize/2)) #place text rect at the middle of the screen.
    gameDisplay.blit(TextSurf, TextRect) #copy image to framebuffer
    pygame.display.update() #update screen


def draw_startup(gameDisplay):
    #just display game start message.
    message_display(gameDisplay,"press space to start the game.")


def draw_cal(gameDisplay):
    message_display(gameDisplay,"press space when disc is centered.")

def draw_height_cal(gameDisplay):
    message_display(gameDisplay,"Lift the box up!")

def height_counter(gameDisplay,count):
    font = pygame.font.SysFont(None, 25)
    text = font.render("Height: " + str(count), True, game_colors.black)
    gameDisplay.blit(text,(xsize/2,ysize/2+50))



def draw_highscore(gameDisplay,game_score):
    message_display(gameDisplay,"Score %d" %game_score)