import pygame
from time import sleep
import re
import os

c_game_sounds_path = "./src/assets/sounds/"

game_sound_dict = {}

sound_height_alert =None

sound_ones_list = []
sound_tens_list = []
sound_hundreds_list = []
sound_elevens_list = []
sound_motivations_list =[]


def init_sounds():
    global game_sound_dict

    generate_dict()
    #step 2: add all numbers to list.
    try:
        sound_ones_list.extend([game_sound_dict['num1'],game_sound_dict['num2'],game_sound_dict['num3'],game_sound_dict['num4'],game_sound_dict['num5'],game_sound_dict['num6'],game_sound_dict['num7'],game_sound_dict['num8'],game_sound_dict['num9']])
        sound_tens_list.extend([game_sound_dict['num10'],game_sound_dict['num20'],game_sound_dict['num30'],game_sound_dict['num40'],game_sound_dict['num50'],game_sound_dict['num60'],game_sound_dict['num70'],game_sound_dict['num80'],game_sound_dict['num90']])
        sound_hundreds_list.extend([game_sound_dict['num100'],game_sound_dict['num200'],game_sound_dict['num300'],game_sound_dict['num400']])
        sound_elevens_list.extend([game_sound_dict['num11'],game_sound_dict['num12'],game_sound_dict['num13'],game_sound_dict['num14'],game_sound_dict['num15'],game_sound_dict['num16'],game_sound_dict['num17'],game_sound_dict['num18'],game_sound_dict['num19']])
        sound_motivations_list.extend([game_sound_dict['motivation1'],game_sound_dict['motivation2'],game_sound_dict['motivation3'],game_sound_dict['motivation4']])
    except KeyError:
        print("sounds not found!")

def generate_dict():
    global game_sound_dict
    for str in os.listdir(c_game_sounds_path):
        if re.search(".wav", str):
            # we now have all sound assets
            name = str.split(".")[0]
            game_sound_dict[name] = pygame.mixer.Sound(c_game_sounds_path+str)

def num_to_sound(num):
    #step 1: generate an empty list for items to play.

    sound_playlist = []
    number_done = False
    num_before = False
    if num>499:
        #opps...
        return

    #step 1: check if number is greater than 100
    if num>=100:
        num_before = True
        #add the hundred number from list and continue
        hun_idx = (int)(num/100) - 1

        sound_playlist.append(sound_hundreds_list.__getitem__(hun_idx))

        #decrement the number and continue.
        num -= (hun_idx+1)*100


        #did we finish and number?
        if num==0:
          number_done = True

    #check the tens
    if num >= 10 and not number_done :
        #test for special case
        if 10 < num < 19:

            #check if there was a number before, if so, we need to add the 'and' sound
            if num_before:
                sound_playlist.append(game_sound_dict['numand'])

            tens_idx = num-10-1
            sound_playlist.append(sound_elevens_list.__getitem__(tens_idx))
            number_done =True
        else:
            tens_idx = (int)(num/10) - 1
            sound_playlist.append(sound_tens_list.__getitem__(tens_idx))
            num -= (tens_idx+1)*10

            if num == 0:
                number_done = True
            else:
                num_before = True
    if num>0 and not number_done:
        if num_before:
            sound_playlist.append(game_sound_dict['numand'])

        sound_playlist.append(sound_ones_list.__getitem__(num-1))

    #now that the number was built. we can add the points and play the list.

    #sound_playlist.append(sound_points)
    for sound in sound_playlist:
        sound.play()
        while pygame.mixer.get_busy():
            sleep(0.05)
