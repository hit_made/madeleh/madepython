import struct
import pi_io_if as i2c


#i2c height addr
i2c_height_addr = 0x22


#i2c position stucts
i2c_height_rx_struct = "I i B B H"
i2c_height_tx_struct = "I I B B H"




height_rx_struct = struct.Struct(i2c_height_rx_struct)
height_tx_struct = struct.Struct(i2c_height_tx_struct)


def i2c_get_curr_height(addr):
    global i2c_bus
    global i2c_rx_msg
    try:
        # attempt to get data from both sensors.
        data = bytes(i2c.i2c_bus.read_i2c_block_data(addr, 0, height_tx_struct.size))
        # verify that data is correct.
        if (i2c.i2c_msg_verify(data,height_rx_struct) == 0):
            return [i2c.i2c_rx_msg[1],i2c.i2c_rx_msg[2]]
        else:
            return [0,0]
    except Exception as e:
            print("Failed To read Sendsor: " + str(addr))
            print(e)
            return [0,0] # if we failed to read the message, tell controller that position was not upadted.


def get_curr_height():
    [height,new_height] = i2c_get_curr_height(i2c_height_addr)
    return [height,new_height]

def reset_height(addr):
    global i2c_bus
    try:
        #build message.
        msg = list(height_tx_struct.pack(i2c.c_i2c_premable,0x00,0x01,0x0,0x0))

        i2c.i2c_bus.write_i2c_block_data(addr,0,msg)

    except Exception as e:
        print("failed to reset dev: " + str(addr))
        print(e)