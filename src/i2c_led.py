import pi_io_if as i2c

i2c_led_addr = 0x40


def init():
    i2c.i2c_bus.write_byte_data(i2c_led_addr,0x00,0x00)


def led_on(leds):
    num = 0x01 << leds*2
    i2c.i2c_bus.write_byte_data(i2c_led_addr,0x0c,num)