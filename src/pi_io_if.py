import pygame
import smbus
import RPi.GPIO as gpio


#consts
c_i2c_premable = 0xfeedabba


c_gpio_btn = 26





i2c_bus = None
i2c_tx_seqnum =0


i2c_rx_msg = []
i2c_tx_msg = []


def main_btn_event(eventtype):
    event = pygame.event.Event(pygame.KEYDOWN, {'key': pygame.K_SPACE})
    print("Button Event Added!")
    pygame.event.post(event)


def init_rpi_gpio():
    gpio.setmode(gpio.BCM)
    gpio.setup(c_gpio_btn,gpio.IN,pull_up_down=gpio.PUD_UP)
    gpio.add_event_detect(c_gpio_btn, gpio.FALLING, callback=main_btn_event, bouncetime=300)


def init_i2c_comm():
    global i2c_bus
    i2c_bus = smbus.SMBus(1)

def calc_checksum(data,size):
    checksum = 0
    for i in range(0,size):
        checksum += data[i]
    return checksum

def i2c_msg_verify(curr_data,data_struct):
    global i2c_rx_msg
    #step 1: verify preamble
    i2c_rx_msg = data_struct.unpack(curr_data)
    if(i2c_rx_msg[0] != c_i2c_premable):
        return 1
    #step 2: test checksum.
    calculated_checksum = calc_checksum(curr_data,data_struct.size - 2)
    if(calculated_checksum != i2c_rx_msg[4]):
        return 1
    return 0

