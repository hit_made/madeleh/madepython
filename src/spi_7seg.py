import spidev
import seven_segment_display

spi = None
display = None

def init_7segspi():
    global display

    # We only have SPI bus 0 available to us on the Pi
    bus = 0

    # Device is the chip select pin. Set to 0 or 1, depending on the connections
    device = 1

    # Enable SPI
    spi = spidev.SpiDev()
    # Open a connection to a specific bus and device (chip select pin)
    spi.open(bus, device)
    # Set SPI speed and mode
    spi.max_speed_hz = 100000
    spi.mode = 0

    display = seven_segment_display.SevenSegmentDisplay(spi)
    display.clear_display()


def num_to_7seg(num):
    global display
    display.write_int(num)
