
import struct
import pi_io_if as i2c
from time import sleep



#encoder i2c addresses

x_encoder_i2c_addr = 0x21
y_encoder_i2c_addr = 0x20

#i2c position stucts
i2c_pos_rx_struct = "I i B B H"
i2c_pos_tx_struct = "I I B B H"

rx_struct = struct.Struct(i2c_pos_rx_struct)
tx_struct = struct.Struct(i2c_pos_tx_struct)


def i2c_get_curr_pos(addr):
    global i2c_bus
    global i2c_rx_msg
    try:
        # attempt to get data from both sensors.
        data = bytes(i2c.i2c_bus.read_i2c_block_data(addr, 0, rx_struct.size))
        # verify that data is correct.
        if (i2c.i2c_msg_verify(data,rx_struct) == 0):
            return [i2c.i2c_rx_msg[1],i2c.i2c_rx_msg[2]]
        else:
            return [0,0]
    except Exception as e:
            print("Failed To read Sendsor: " + str(addr))
            print(e)
            return [0,0] # if we failed to read the message, tell controller that position was not upadted.


def get_curr_pos():
    [x_pos,new_x] = i2c_get_curr_pos(x_encoder_i2c_addr)
    [y_pos,new_y] = i2c_get_curr_pos(y_encoder_i2c_addr)
    #print("ypos: " + str(y_pos) + " newpos: " + str(new_y))
    return [x_pos,new_x,-y_pos,new_y]



def reset_pos(addr):
    global i2c_bus
    try:
        #build message.
        msg = list(tx_struct.pack(i2c.c_i2c_premable,0x00,0x01,0x0,0x0))

        i2c.i2c_bus.write_i2c_block_data(addr,0,msg)

    except Exception as e:
        print("failed to reset dev: " + str(addr))
        print(e)


def send_cal_pos(addr,cal_val):
    global i2c_bus

    try:
        msg = list(tx_struct.pack(i2c.c_i2c_premable,cal_val,0x02,0x0,0x0))
        i2c.i2c_bus.write_i2c_block_data(addr,0,msg)

    except Exception as e:
        print("failed to update cal dev: " + str(addr))
        print(e)