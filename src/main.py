# made ball game code

import pygame
from enum import Enum
import game_colors
import pi_io_if
import game_graphics
import game_sounds
import i2c_RopeEncoder as RopeEnc
import i2c_height as HeightMeas
import led_controller as leds
import spi_7seg as _7seg
import i2c_led
import math
from random import randint
from os import uname
import sys
import ctypes
from time import sleep


# typedefs
class game_state_type(Enum):
    Init_state = 0
    Startup_state = 1
    Cal_state = 2
    Game_state = 3
    Highscore_state = 4
    Batteryalert_state = 5
    Height_test_state = 6


class diffculty_state_type(Enum):
    Stop_state = 0
    Quick_test_state = 1
    General_testing_state = 2


# ----------------------------------
game_main_btn = pygame.K_SPACE
game_height_alert_btn = pygame.K_q

c_frame_led_update_dly = 20
frame_led_dly = 0

c_quickplayer_firstscore_time = 55
c_normalplayer_score_delta = 10

c_radius_difficulty_factor = 0.95
c_common_point_deltaT_low = 5
c_common_point_deltaT_high = 15
c_breath_delay = 5

last_point_grow_t =0
c_point_grow_dly  = 10

new_score_flag = 0

is_rpi = 0

# setup the parameters
curr_game_state = game_state_type.Init_state
curr_difficulty_mgmt_state = diffculty_state_type.Stop_state
curr_game_diffculty = 1
curr_game_color_locs = []
curr_color_idx = 0
curr_color = []

last_point_t = 0
curr_game_points = 0
curr_point_t = 0
curr_game_time = 0
max_radius = 120

curr_radius = game_graphics.init_radius

disc_xpos = 0
disc_ypos = 0
disc_zpos = 0
disc_points = []
discimg_size = 50

x_change = 0
y_change = 0
min_point_idx = 0

stable_height_count = 0

c_min_height = 500
c_min_stable_counts = 7

height_stable = False


last_sound_time = 0
c_last_sound_time_dly = 360
c_motivation_dly = 300

# init pygame handler
pygame.mixer.pre_init(44100, 16, 2, 4096)
pygame.init()

# set display size
gameDisplay = pygame.display.set_mode((game_graphics.xsize, game_graphics.ysize))

# set title
pygame.display.set_caption('handball game')

# set gameclock
clock = pygame.time.Clock()

# assets
discimg = pygame.image.load("./src/assets/ball_img.png")

ledsOn = []



def init_game():
    # todo: add battery test here
    global curr_game_state
    global curr_difficulty_mgmt_state
    curr_game_state = game_state_type.Startup_state
    curr_difficulty_mgmt_state = diffculty_state_type.Stop_state
    game_sounds.init_sounds()


def sound_task():
    global curr_game_state
    global curr_game_points
    global disc_zpos
    global curr_game_time
    global last_point_t
    global last_sound_time
    global c_last_sound_time_dly


    if last_sound_time == 0:
        if curr_game_state == game_state_type.Cal_state:
            game_sounds.game_sound_dict['center_cmd'].play()
            last_sound_time = c_last_sound_time_dly
        elif curr_game_state == game_state_type.Height_test_state:
            game_sounds.game_sound_dict['pull_rope_cmd'].play()
            last_sound_time = c_last_sound_time_dly
        elif curr_game_state == game_state_type.Game_state:
            game_sounds.sound_motivations_list[randint(0,3)].play()
            last_sound_time = c_motivation_dly
        elif curr_game_state == game_state_type.Highscore_state:
            game_sounds.num_to_sound(curr_game_points)
            last_sound_time = c_motivation_dly
    else:
        last_sound_time -= 1



def height_task():
    # this task takes care of manging that the box is held upright and high.
    # todo: should timer be paused if height is unsable?
    test_stable_height()


def diffculty_task():
    global curr_radius
    global curr_game_diffculty
    global curr_game_state
    global curr_difficulty_mgmt_state
    global last_point_t
    global curr_point_t
    global curr_game_points
    global curr_game_time
    global new_score_flag
    global last_point_grow_t

    if curr_game_state == game_state_type.Game_state:

        if curr_difficulty_mgmt_state == diffculty_state_type.Stop_state:
            curr_game_diffculty = 1
            curr_difficulty_mgmt_state = diffculty_state_type.Quick_test_state
        elif curr_difficulty_mgmt_state == diffculty_state_type.Quick_test_state:
            if new_score_flag:

                new_score_flag = 0

                if curr_point_t > c_quickplayer_firstscore_time:
                    curr_game_diffculty = 2
                    curr_radius = curr_radius / (curr_game_diffculty * c_radius_difficulty_factor)
                    print("game hard")
                    curr_difficulty_mgmt_state = diffculty_state_type.General_testing_state
                else:
                    curr_game_diffculty = 1
                    curr_difficulty_mgmt_state = diffculty_state_type.General_testing_state
        elif curr_difficulty_mgmt_state == diffculty_state_type.General_testing_state:
            if new_score_flag:

                # clear the new score flag.
                new_score_flag = 0

                if last_point_t - curr_point_t < c_common_point_deltaT_low:
                    curr_radius = curr_radius / (curr_game_diffculty * c_radius_difficulty_factor)

                    if curr_radius < 2:
                        curr_radius = 2
                    curr_game_diffculty += 1
                elif last_point_t - curr_point_t > c_common_point_deltaT_high:
                    if curr_game_diffculty == 1:
                        curr_radius = max_radius
                        curr_game_diffculty = 1
                    else:
                        curr_radius = curr_radius * (curr_game_diffculty * c_radius_difficulty_factor)
                        #make sure that the diffcultly solve doesn't go too far
                        if curr_radius > max_radius:
                            curr_radius = max_radius
                        curr_game_diffculty -= 1
                print(curr_radius)
            
            elif curr_point_t - curr_game_time > c_breath_delay:
                if last_point_grow_t > c_point_grow_dly:
                    last_point_grow_t =0
                    if curr_radius > max_radius:
                        curr_radius = max_radius
                    else:
                        curr_radius = curr_radius*1.05
                else:
                    last_point_grow_t += 1
                  
            elif curr_game_state == game_state_type.Highscore_state:
                curr_difficulty_mgmt_state = diffculty_state_type.Stop_state

def start_new_game():
    global curr_game_state
    global curr_game_color_locs
    global curr_game_points
    global curr_game_time
    global curr_radius

    curr_game_state = game_state_type.Height_test_state
    curr_game_time = 120
    curr_game_points = 0
    curr_radius = game_graphics.init_radius

    pygame.time.set_timer(pygame.USEREVENT, 1000)


def test_stable_height():
    global disc_zpos
    global stable_height_count
    global height_stable
    # step2: check if the height is actually high enough
    if disc_zpos > c_min_height:
        if stable_height_count > c_min_stable_counts:
            # all good, start the game
            height_stable = True
        else:
            # reset counter.
            stable_height_count = stable_height_count + 1
    else:
        stable_height_count = 0
        height_stable = 0


def game_score():
    global curr_game_state
    global curr_game_points
    global curr_game_diffculty
    global curr_game_time
    global curr_point_t
    global last_point_t
    global curr_color_idx
    global curr_color

    game_sounds.game_sound_dict['game_score'].play()
    curr_game_points += curr_game_diffculty
    new_color_idx = randint(0, len(curr_game_color_locs) - 1)
    while new_color_idx == curr_color_idx:
        new_color_idx = randint(0, len(curr_game_color_locs) - 1)
    curr_color_idx = new_color_idx

    if is_rpi:
        leds.fill_leds(game_colors.red)
        sleep(0.2)
        leds.fill_leds(game_colors.yellow)
        sleep(0.2)
        leds.fill_leds(game_colors.green)
        sleep(0.2)
        leds.fill_leds(game_colors.blue)
        sleep(0.2)
        leds.fill_leds(game_colors.red)
        sleep(0.2)
        leds.fill_leds(game_colors.green)
        sleep(0.2)
        leds.clear_leds()
        
        _7seg.num_to_7seg(curr_game_points)
    last_point_t = curr_point_t
    curr_point_t = curr_game_time


def generate_cirle_points():
    global disc_xpos
    global disc_ypos
    global disc_points
    global discimg_size

    # step 1: build a list of points around zero axis.

    arg_res = (2 * math.pi) / 4

    for i in range(1, 4 + 1):
        point_y = round((discimg_size / 2) * math.cos(arg_res * i))
        point_x = round((discimg_size / 2) * math.sin(arg_res * i))
        disc_points.append([point_x, point_y])


def handle_keyboard_input(event):
    global curr_game_state
    global curr_game_time
    global curr_color_idx
    global curr_game_color_locs
    global curr_color
    global x_change
    global y_change
    global disc_ypos
    global disc_xpos
    global disc_zpos
    global disc_points
    global disable_io

    if curr_game_state == game_state_type.Startup_state:
        if event.type == pygame.KEYDOWN:
            if event.key == game_main_btn:
                curr_game_color_locs = game_colors.generate_color_list(game_graphics.xsize - 150,
                                                                       game_graphics.ysize - 150, 100)
                curr_game_state = game_state_type.Cal_state
    elif curr_game_state == game_state_type.Height_test_state:
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFTBRACKET:
                disc_zpos += 20
            elif event.key == pygame.K_RIGHTBRACKET:
                disc_zpos -= 20
    elif curr_game_state == game_state_type.Cal_state:
        if event.type == pygame.KEYDOWN:
            if event.key == game_main_btn:
                disc_xpos = game_graphics.xsize / 2
                disc_ypos = game_graphics.ysize / 2
                generate_cirle_points()
                curr_color = curr_game_color_locs.__getitem__(curr_color_idx)

                if is_rpi and not disable_io:
                    RopeEnc.reset_pos(RopeEnc.x_encoder_i2c_addr)
                    RopeEnc.reset_pos(RopeEnc.y_encoder_i2c_addr)
                    RopeEnc.reset_pos(RopeEnc.x_encoder_i2c_addr)
                    RopeEnc.reset_pos(RopeEnc.y_encoder_i2c_addr)
                    RopeEnc.send_cal_pos(RopeEnc.x_encoder_i2c_addr,ctypes.c_ulong(-11460).value)
                start_new_game()
        #elif event.type == pygame.KEYUP:
        #    if event.key == game_main_btn:
    elif curr_game_state == game_state_type.Highscore_state:
        if event.type == pygame.KEYDOWN:
            if event.key == game_main_btn:
                curr_game_state = game_state_type.Startup_state

    # key event handling.
    if event.type == pygame.KEYDOWN:  # is any key pressed?
        if event.key == pygame.K_LEFT:
            x_change = -game_graphics.step_size
        elif event.key == pygame.K_RIGHT:
            x_change = game_graphics.step_size

        if event.key == pygame.K_UP:
            y_change = -game_graphics.step_size
        elif event.key == pygame.K_DOWN:
            y_change = game_graphics.step_size

    if event.type == pygame.KEYUP:  # was any key released?
        if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
            x_change = 0
        if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
            y_change = 0


def select_closest_point():
    global curr_color
    global disc_points
    global disc_xpos
    global disc_ypos

    minp = math.hypot(game_graphics.xsize, game_graphics.ysize)
    curr_min_idx = 0
    for i in disc_points:
        idx_dist = math.hypot((i.__getitem__(0) + disc_xpos) - curr_color.__getitem__(1),
                              (i.__getitem__(1) + disc_ypos) - curr_color.__getitem__(2))

        if idx_dist < minp:
            minp = idx_dist
            curr_min_idx = disc_points.index(i)

    return curr_min_idx


def pygame_main():
    global curr_game_state
    global curr_game_color_locs
    global curr_game_points
    global disc_ypos
    global disc_xpos
    global disc_zpos
    global x_change
    global y_change
    global curr_game_time
    global curr_color_idx
    global curr_color
    global last_point_t
    global curr_point_t
    global new_score_flag
    global ledsOn
    global min_point_idx
    # get all events from game
    for event in pygame.event.get():
        # check if user asked to close the game.
        if event.type == pygame.QUIT:
            pygame.quit()

            if is_rpi:
                leds.clear_leds()
                leds.update_leds()
            quit()

        if curr_game_state == game_state_type.Game_state:
            if event.type == pygame.USEREVENT:
                curr_game_time = curr_game_time - 1

        handle_keyboard_input(event)

    # handle new position from
    disc_xpos += x_change
    disc_ypos += y_change

    # tutorial stage handling
    if curr_game_state == game_state_type.Height_test_state:
        # stay here until height is stable.
        if height_stable:
            curr_game_state = game_state_type.Game_state
            game_sounds.game_sound_dict['red_arrow_cmd'].play()

    # scoring!
    if curr_game_state == game_state_type.Game_state:
        curr_dist = math.hypot(disc_xpos - curr_color.__getitem__(1), disc_ypos - curr_color.__getitem__(2))

        if curr_dist < curr_radius + (discimg_size / 2):
            new_score_flag = 1
            game_score()
    # game over mgmt
    if curr_game_state == game_state_type.Game_state:
        if curr_game_time == 0:
            # game has ended, display ending.
            curr_game_state = game_state_type.Highscore_state

    # game state graphics handling.
    gameDisplay.fill(game_colors.med_gray)

    if curr_game_state == game_state_type.Startup_state:
        game_graphics.draw_startup(gameDisplay)
    elif curr_game_state == game_state_type.Cal_state:
        game_graphics.draw_cal(gameDisplay)
        # ----------------------------------------debug--------------------------------------------
        for i in curr_game_color_locs:
            game_graphics.things(gameDisplay, i.__getitem__(1), i.__getitem__(2), 10, i.__getitem__(0))
        # -----------------------------------------------------------------------------------------

        curr_color_idx = randint(0, len(curr_game_color_locs) - 1)
    elif curr_game_state == game_state_type.Height_test_state:
        game_graphics.draw_height_cal(gameDisplay)
        game_graphics.height_counter(gameDisplay, disc_zpos)

    elif curr_game_state == game_state_type.Game_state:
        game_graphics.place_image(gameDisplay, discimg, disc_xpos - discimg_size / 2, disc_ypos - discimg_size / 2)
        game_graphics.score_counter(gameDisplay, curr_game_points)
        game_graphics.time_counter(gameDisplay, curr_game_time)

        # debug
        for i in disc_points:
            game_graphics.things(gameDisplay, int(i.__getitem__(0) + disc_xpos), int(i.__getitem__(1) + disc_ypos),
                                 3, game_colors.black)

        min_point_idx = select_closest_point()
        min_point_item = disc_points[min_point_idx]
        game_graphics.things(gameDisplay, int(min_point_item.__getitem__(0) + disc_xpos),
                             int(min_point_item.__getitem__(1) + disc_ypos),
                             3, game_colors.red)

        # draw current use thing
        game_graphics.things(gameDisplay, int(curr_color.__getitem__(1)), int(curr_color.__getitem__(2)), curr_radius,
                             curr_color.__getitem__(0))
        curr_color = curr_game_color_locs.__getitem__(curr_color_idx)

    elif curr_game_state == game_state_type.Highscore_state:
        game_graphics.draw_highscore(gameDisplay, curr_game_points)
    # move framebuffer to screen.
    pygame.display.update()

    # target this fps
    clock.tick(60)


def request_position():
    global curr_game_state
    global disc_xpos
    global disc_ypos

    if curr_game_state == game_state_type.Game_state:
        [xpos, newx, ypos, newy] = RopeEnc.get_curr_pos()
        if newx == 1:
            disc_xpos = game_graphics.xsize / 2 + xpos
        if newy == 1:
            disc_ypos = game_graphics.ysize / 2 + ypos


def request_height():
    global curr_game_state
    global disc_zpos
    if curr_game_state == game_state_type.Height_test_state:
        [height, new_height] = HeightMeas.get_curr_height()
        if new_height:
            disc_zpos = height


def update_leds():
    global curr_game_state
    global ledsOn
    global curr_color
    global min_point_idx
    global frame_led_dly

    if frame_led_dly > c_frame_led_update_dly:
        if curr_game_state != game_state_type.Game_state:
            leds.clear_leds()
        #leds.update_leds()
        i2c_led.led_on(min_point_idx)
        frame_led_dly = 0
    else:
        frame_led_dly = frame_led_dly + 1


def main_loop():
    global ledsOn
    global disable_io

    gameExit = False
    while not gameExit:
        height_task()
        diffculty_task()
        sound_task()
        if is_rpi and not disable_io:
            request_position()
            request_height()
        pygame_main()
        if is_rpi:
            update_leds()


if len(sys.argv) > 1:
    if sys.argv[1] == "-d":
        disable_io = True
        print("i/o Disabled!")
    else:
        disable_io = False
else:
    disable_io = False

# test if game is running on a pi.
if uname().nodename == 'madeball':
    is_rpi = 1
    print("hi pi.")
    pi_io_if.init_i2c_comm()
    pi_io_if.init_rpi_gpio()
    leds.init_leds()
    i2c_led.init()
    _7seg.init_7segspi()

init_game()

main_loop()
