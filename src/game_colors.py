import random
from math import ceil
from math import log2
#const



###
#Medium Gray	 	128	 	128	 	128
#Aqua	 	0	 	128	 	128
#Navy Blue	 	0	 	0	 	128
#Green	 	0	 	255	 	0
#Orange	 	255	 	165	 	0
#Yellow	 	255	 	255	 	0




black = (0,0,0)
white = (255,255,255)
med_gray = (128,128,128)
red =  (255,0,0)
green = (0,255,0)
blue = (0,0,255)
yellow = (255,255,0)
aqua = (0,128,128)
orange = (255,165,0)


color_list= [yellow,orange,orange,orange,orange,orange,blue,orange,orange,orange,orange,red,green,blue,aqua,orange]

def requiredbits(num):
    return pow(2,ceil(log2(num)))


#divide the screen into quadrents in normalized size of colors,
#the location will be randomly selected from inside the quadrant.

def generate_color_list(xmax,ymax,edge_offset):
    game_color_list = []
    ix=0
    iy=0
    num_of_colors =len(color_list)


    #round the num of colors to closest 2 over n number.
    num_of_quadrants = requiredbits(num_of_colors)

    #the larger dimm gets more sectors.
    if xmax>ymax:
        x_step = xmax/(num_of_quadrants/2)
        y_step = ymax/(num_of_quadrants/4)
    elif ymax>xmax:
        x_step = xmax / (num_of_quadrants / 4)
        y_step = ymax / (num_of_quadrants / 2)
    else:
        x_step = xmax / (num_of_quadrants / 2)
        y_step = ymax / (num_of_quadrants / 2)
        is_interlaced = 1

    x_step = round(x_step)
    y_step = round(y_step)

    #step 1: shuffle the list of colors.
    random.shuffle(color_list)

    # for each color, generate a tuple list with a random position.
    last_x = 0
    last_y = 0
    for i in color_list:

        if is_interlaced:

            #perform interlacing.
            if(last_x == 0):
                x_pos = random.randint(ix*x_step+edge_offset,(ix+1)*x_step+edge_offset)
                y_pos = random.randint(iy * y_step + edge_offset, (iy + 1) * y_step + edge_offset)
                last_x += 1
            elif last_x == 1:
                y_pos = random.randint((iy+2)*y_step+edge_offset,(iy+3)*y_step+edge_offset) #fixme: this is tuned to 16 indexes...
                last_x = 0
        else:
            x_pos = random.randint(ix * x_step + edge_offset, (ix + 1) * x_step+edge_offset)
            y_pos = random.randint(iy * y_step + edge_offset, (iy + 1) * y_step+edge_offset)

#debug
#        print(str(ix)+' '+ str(iy)+ ' ' + str(x_pos)+ ' ' +str(y_pos))

        game_color_list.append((i,x_pos,y_pos))



        if(ix >= ((num_of_quadrants/2 )-1)):
            if is_interlaced:
                ix =0
                iy= iy+4; #fixme: this as well.
            else:
                ix =0
                iy= iy+1;
        else:
            ix = ix+1;

    return game_color_list






